import os
import sys
from distutils.core import setup
import py2exe  # noqa: F401

sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(__file__)), 'source'))

setup(
    # options = {'py2exe': {'bundle_files': 1}},
    # zipfile=None,
    data_files=[
       ('locale/en/LC_MESSAGES', ["source/locale/en/LC_MESSAGES/messages.mo"]),
       ('locale/ru/LC_MESSAGES', ["source/locale/ru/LC_MESSAGES/messages.mo"]),
    ],
    windows=[{
      "script": "source/app.py",
      "icon_resources": [(1, "gpas.ico")],
      "dest_base": "zoom_graphs",
    }]
)
