import os
from ConfigParser import SafeConfigParser


def b2i(bool_val):
    return (1 if bool_val else 0)


class Config(SafeConfigParser):

    def __init__(self, main_window):
        SafeConfigParser.__init__(self)

        self.main_window = main_window
        self.ini = os.path.join(os.path.expanduser("~"), 'zoom_graphs.ini')
        self.read(self.ini)

        for sect in ["MainWindow", "Vertical", "Horizontal"]:
            if not self.has_section(sect):
                self.add_section(sect)

    def save(self):
        sect = "MainWindow"
        posx, posy = self.main_window.GetPosition()
        sizex, sizey = self.main_window.GetSize()
        self.set(sect, "posx", "{}".format(posx))
        self.set(sect, "posy", "{}".format(posy))
        self.set(sect, "sizex", "{}".format(sizex))
        self.set(sect, "sizey", "{}".format(sizey))
        self.set(sect, "sashpos", "{}".format(self.main_window.sp.GetSashPosition()))

        sect = "Vertical"
        g = self.main_window.ver_graphs
        self.set(sect, "settings", "{}".format(b2i(g.show_settings.IsChecked())))
        w = g.settings_panel
        self.set(sect, "moved", "{}".format(b2i(g.is_sensor)))
        self.set(sect, "border_top", "{}".format(b2i(w.graph_bord_top.IsChecked())))
        self.set(sect, "border_bottom", "{}".format(b2i(w.graph_bord_bot.IsChecked())))
        self.set(sect, "sel_calcked", "{}".format(b2i(w.graph_calck.IsChecked())))
        self.set(sect, "sel_top", "{}".format(b2i(w.graph_sel_top.IsChecked())))
        self.set(sect, "sel_bottom", "{}".format(b2i(w.graph_sel_bot.IsChecked())))

        sect = "Horizontal"
        g = self.main_window.hor_graphs
        self.set(sect, "settings", "{}".format(b2i(g.show_settings.IsChecked())))
        w = g.settings_panel
        self.set(sect, "moved", "{}".format(b2i(g.is_sensor)))
        self.set(sect, "border_top", "{}".format(b2i(w.graph_bord_top.IsChecked())))
        self.set(sect, "border_bottom", "{}".format(b2i(w.graph_bord_bot.IsChecked())))
        self.set(sect, "sel_calcked", "{}".format(b2i(w.graph_calck.IsChecked())))
        self.set(sect, "sel_top", "{}".format(b2i(w.graph_sel_top.IsChecked())))
        self.set(sect, "sel_bottom", "{}".format(b2i(w.graph_sel_bot.IsChecked())))

        with open(self.ini, 'wb') as configfile:
            self.write(configfile)

    def readint(self, sect, key, dflt):
        if self.has_option(sect, key):
            return self.getint(sect, key)

        return dflt

    def readbool(self, sect, key, dflt):
        v = 1 if dflt else 0
        v = self.readint(sect, key, v)
        return (v != 0)

    def restore(self):
        sect = "MainWindow"
        posx = self.readint(sect, "posx", -1)
        posy = self.readint(sect, "posy", -1)
        sizex = self.readint(sect, "sizex", 800)
        sizey = self.readint(sect, "sizey", 600)
        sashpos = self.readint(sect, "sashpos", 300)

        self.main_window.SetPosition((posx, posy))
        self.main_window.SetSize((sizex, sizey))
        self.main_window.sp.SetSashPosition(sashpos)

        sect = "Vertical"
        g = self.main_window.ver_graphs
        g.show_settings.SetValue(self.readbool(sect, "settings", True))
        g.set_is_sensor(self.readbool(sect, "moved", True))
        g.set_border_top(self.readbool(sect, "border_top", True))
        g.set_border_bottom(self.readbool(sect, "border_bottom", True))
        g.set_sel_calcked(self.readbool(sect, "sel_calcked", True))
        g.set_sel_top(self.readbool(sect, "sel_top", True))
        g.set_sel_bottom(self.readbool(sect, "sel_bottom", True))
        g.OnSettings(None)

        sect = "Horizontal"
        g = self.main_window.hor_graphs
        g.show_settings.SetValue(self.readbool(sect, "settings", True))
        g.set_is_sensor(self.readbool(sect, "moved", True))
        g.set_border_top(self.readbool(sect, "border_top", True))
        g.set_border_bottom(self.readbool(sect, "border_bottom", True))
        g.set_sel_calcked(self.readbool(sect, "sel_calcked", True))
        g.set_sel_top(self.readbool(sect, "sel_top", True))
        g.set_sel_bottom(self.readbool(sect, "sel_bottom", True))
        g.OnSettings(None)
