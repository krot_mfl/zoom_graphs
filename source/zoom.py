class Zoom(object):

    def __init__(self, json_data):
        self.krot = json_data
        self.selection = json_data.get("selection", {})

        krot_data = json_data["dataRow"]
        self.data = [list(reversed(row)) for row in zip(*krot_data[::-1])]

        if self.selection:
            if self.selection["x_right"] >= len(self.data[0]):
                self.selection["x_right"] = len(self.data[0]) - 1
            if self.selection["y_bottom"] >= len(self.data):
                self.selection["y_bottom"] = len(self.data) - 1

        self.val_min = self.data[0][0]
        self.val_max = self.val_min

        for row in self.data:
            for val in row:
                if val < self.val_min:
                    self.val_min = val
                if val > self.val_max:
                    self.val_max = val

    def rotated(self):
        return [list(reversed(row)) for row in zip(*self.data[::-1])]
