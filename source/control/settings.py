import wx


class Settings(wx.Panel):

    def __init__(self, parent, is_hor):
        super(Settings, self).__init__(parent, -1)

        self.SetBackgroundColour('black')

        self.graph_moved = self.cbox(
          _("Dynamic row") if is_hor else _("Dynamic column"),
          'yellow'
        )
        self.graph_bord_top = self.cbox(
          _("Zoom top border") if is_hor else _("Zoom left border"),
          'white'
        )
        self.graph_bord_bot = self.cbox(
          _("Zoom bottom border") if is_hor else _("Zoom right border"),
          'gray'
        )
        self.graph_calck = self.cbox(
          _("Calculated row") if is_hor else _("Calculated column"),
          'red'
        )
        self.graph_sel_top = self.cbox(
          _("Selection top border") if is_hor else _("Selection left border"),
          'green'
        )
        self.graph_sel_bot = self.cbox(
          _("Selection bottom border") if is_hor else _("Selection right border"),
          'dark green'
        )

        szr = wx.BoxSizer(wx.VERTICAL)
        szr.Add(self.graph_moved, 0, wx.EXPAND | wx.ALL, 5)
        szr.Add(self.graph_calck, 0, wx.EXPAND | wx.ALL, 5)
        szr.Add(self.graph_bord_top, 0, wx.EXPAND | wx.ALL, 5)
        szr.Add(self.graph_bord_bot, 0, wx.EXPAND | wx.ALL, 5)
        szr.Add(self.graph_sel_top, 0, wx.EXPAND | wx.ALL, 5)
        szr.Add(self.graph_sel_bot, 0, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(szr)
        szr.Fit(self)

    def cbox(self, caption, color):
        checkbox = wx.CheckBox(self, -1, caption)
        checkbox.SetForegroundColour(color)
        return checkbox
