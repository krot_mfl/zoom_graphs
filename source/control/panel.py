import wx


class Panel(wx.Panel):

    def __init__(self, parent):
        super(Panel, self).__init__(parent, -1)
        self.top_border = wx.StaticText(self, -1, "0")
        self.low_border = wx.StaticText(self, -1, "0")
        self.diapazon = wx.StaticText(self, -1, "0")
        self.caption = wx.StaticText(self, -1, _("Borders"))

        box = wx.GridSizer(rows=3, cols=1, hgap=2, vgap=2)
        box.Add(self.top_border, 0, wx.ALIGN_TOP | wx.RIGHT, 5)
        box.Add(self.diapazon, 0)
        box.Add(self.low_border, 0, wx.ALIGN_BOTTOM)

        outbox = wx.BoxSizer(wx.VERTICAL)
        outbox.Add(self.caption, 0, wx.EXPAND | wx.RIGHT, 5)
        outbox.Add(box, 1, wx.EXPAND)

        self.SetSizer(outbox)
        outbox.Fit(self)

    def set_borders(self, min_val, max_val):
        self.top_border.SetLabel("{}".format(max_val))
        self.low_border.SetLabel("{}".format(min_val))
        self.diapazon.SetLabel("{}".format(max_val - min_val))
