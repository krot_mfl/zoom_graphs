import wx

from .canvas import Canvas
from .panel import Panel
from .settings import Settings


class Graphs(wx.Panel):

    def __init__(self, parent, is_horizontal, data, vmin, vmax, mmsize, sel_top, sel_calcked, sel_bottom):
        super(Graphs, self).__init__(parent, -1, style=wx.RAISED_BORDER)

        self.mirror = None
        self.charts = {}
        self.projections = {}

        self.sel_top = sel_top
        self.sel_calcked = sel_calcked
        self.sel_bottom = sel_bottom

        title = _("Graph") if is_horizontal else _("Shear")

        self.data = data
        self.size_mm = mmsize
        self.size_x = len(self.data[0])

        self.canvas = Canvas(self)
        self.canvas_panel = Panel(self)
        self.settings_panel = Settings(self, is_horizontal)

        self.set_border_top(True)
        self.set_border_bottom(True)
        self.set_is_sensor(True)

        canvas_box = wx.BoxSizer(wx.HORIZONTAL)
        canvas_box.Add(self.canvas_panel, 0, wx.EXPAND)
        canvas_box.Add(self.canvas, 1, wx.EXPAND)
        canvas_box.Add(self.settings_panel, 0, wx.EXPAND)

        self.pos_title = wx.StaticText(self, -1, _("Pos"))
        self.val_title = wx.StaticText(self, -1, _("Val"))

        tmp, text_height = self.pos_title.GetSize()
        panel_width, tmp = self.canvas_panel.GetSize()
        self.title = wx.StaticText(self, -1, title, size=(panel_width, text_height))
        # print "###", s1, s2

        self.show_settings = wx.CheckBox(self, -1, _("Settings"))
        self.pos_value = wx.StaticText(self, -1, "0", size=(50, text_height))
        self.val_value = wx.StaticText(self, -1, "0", size=(50, text_height))

        if self.sel_calcked is None:
            v = 0
        else:
            v = self.sel_calcked

        self.sensor = wx.Slider(
          self, -1, v, 0, len(self.data) - 1, style=wx.SL_HORIZONTAL
        )
        self.sens_value = wx.StaticText(self, -1, str(self.sel_calcked), size=(50, text_height))

        title_box = wx.BoxSizer(wx.HORIZONTAL)
        title_box.Add(self.title, 0, wx.EXPAND)
        title_box.Add(self.show_settings, 0, wx.EXPAND | wx.LEFT, 5)
        title_box.Add(self.pos_title, 0, wx.EXPAND | wx.LEFT, 15)
        title_box.Add(self.pos_value, 0, wx.EXPAND | wx.LEFT, 5)
        title_box.Add(self.val_title, 0, wx.EXPAND | wx.LEFT, 5)
        title_box.Add(self.val_value, 0, wx.EXPAND | wx.LEFT, 5)
        title_box.Add(self.sensor, 0, wx.EXPAND | wx.LEFT, 5)
        title_box.Add(self.sens_value, 0, wx.EXPAND | wx.LEFT, 5)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(title_box, 0, wx.EXPAND)
        box.Add(canvas_box, 1, wx.EXPAND)

        self.SetSizer(box)
        box.Fit(self)

        self.canvas.Bind(wx.EVT_MOTION, self.OnMotion)
        self.sensor.Bind(wx.EVT_SCROLL, self.OnSensor)
        self.show_settings.Bind(wx.EVT_CHECKBOX, self.OnSettings)

        self.settings_panel.graph_moved.Bind(wx.EVT_CHECKBOX, self.OnYellow)
        self.settings_panel.graph_bord_top.Bind(wx.EVT_CHECKBOX, self.OnBorderTop)
        self.settings_panel.graph_bord_bot.Bind(wx.EVT_CHECKBOX, self.OnBorderBottom)
        self.settings_panel.graph_calck.Bind(wx.EVT_CHECKBOX, self.OnSelectionCalck)
        self.settings_panel.graph_sel_top.Bind(wx.EVT_CHECKBOX, self.OnSelectionTop)
        self.settings_panel.graph_sel_bot.Bind(wx.EVT_CHECKBOX, self.OnSelectionBottom)

        self.set_borders(vmin, vmax)
        self.canvas.InitBuffer()

    def OnYellow(self, event):
        self.set_is_sensor(self.settings_panel.graph_moved.IsChecked())
        self.Refresh()
        event.Skip()

    def OnBorderTop(self, event):
        self.set_border_top(self.settings_panel.graph_bord_top.IsChecked())
        self.Refresh()
        event.Skip()

    def OnBorderBottom(self, event):
        self.set_border_bottom(self.settings_panel.graph_bord_bot.IsChecked())
        self.Refresh()
        event.Skip()

    def OnSelectionCalck(self, event):
        self.set_sel_calcked(self.settings_panel.graph_calck.IsChecked())
        self.Refresh()
        event.Skip()

    def OnSelectionTop(self, event):
        self.set_sel_top(self.settings_panel.graph_sel_top.IsChecked())
        self.Refresh()
        event.Skip()

    def OnSelectionBottom(self, event):
        self.set_sel_bottom(self.settings_panel.graph_sel_bot.IsChecked())
        self.Refresh()
        event.Skip()

    def Redraw(self):
        self.canvas.InitBuffer()
        self.canvas.Refresh(False)

    def Refresh(self):
        self.Redraw()
        self.mirror.Redraw()

    def OnSensor(self, event):
        self.sens_value.SetLabel("{}".format(self.sensor.GetValue()))
        self.Refresh()

    def OnSettings(self, event):
        self.settings_panel.Show(self.show_settings.IsChecked())
        self.Layout()
        if event:
            event.Skip()

    def OnMotion(self, event):
        h, v = event.GetPosition()
        self.pos_value.SetLabel("{} {}".format(self.canvas.pix2mm(h), _("mm")))
        self.val_value.SetLabel("{} {}".format(int(self.canvas.pix2y(v)), _("ADC")))
        event.Skip()

    def set_is_sensor(self, is_on):
        self. is_sensor = is_on
        self.settings_panel.graph_moved.SetValue(is_on)

    def set_border_top(self, is_on):
        if is_on:
            self.add_chart(0, params={'color': 'white'})
        else:
            self.del_chart(0)

        self.settings_panel.graph_bord_top.SetValue(is_on)

    def set_border_bottom(self, is_on):
        indx = len(self.data) - 1
        if is_on:
            self.add_chart(indx, params={'color': 'gray'})
        else:
            self.del_chart(indx)

        self.settings_panel.graph_bord_bot.SetValue(is_on)

    def set_selection(self, is_on, params, index, checkbox, is_graph_draw=True):
        if index is None:
            return

        if is_on:
            if is_graph_draw:
                self.add_chart(index, params=params)
            self.mirror.add_projection(index, params=params)
        else:
            if is_graph_draw:
                self.del_chart(index)
            self.mirror.del_projection(index)

        checkbox.SetValue(is_on)

    def set_sel_calcked(self, is_on):
        self.set_selection(
          is_on, {'color': 'red'}, self.sel_calcked, self.settings_panel.graph_calck
        )

    def set_sel_top(self, is_on):
        self.set_selection(
          is_on, {'color': 'green'}, self.sel_top,
          self.settings_panel.graph_sel_top, is_graph_draw=False
        )

    def set_sel_bottom(self, is_on):
        self.set_selection(
          is_on, {'color': 'dark green'}, self.sel_bottom,
          self.settings_panel.graph_sel_bot, is_graph_draw=False
        )

    def set_borders(self, min_val, max_val):
        self.y_min = min_val
        self.y_max = max_val
        self.canvas_panel.set_borders(self.y_min, self.y_max)

    def add_projection(self, col_index, params=None):
        if (col_index < 0) or (col_index >= len(self.data[0])):
            raise Exception("Wrong add_projection col_index: {}".format(col_index))

        if col_index in self.projections:
            return

        if not params:
            params = {}

        self.projections[col_index] = params

    def del_projection(self, col_index):
        if col_index in self.projections:
            del self.projections[col_index]

    def add_chart(self, row_index, params=None):
        if (row_index < 0) or (row_index >= len(self.data)):
            raise Exception("Wrong add_chart row_index: {}".format(row_index))

        if row_index in self.charts:
            return

        if not params:
            params = {}

        self.charts[row_index] = params

    def del_chart(self, row_index):
        if row_index in self.charts:
            del self.charts[row_index]
