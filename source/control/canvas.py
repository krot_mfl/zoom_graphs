import warnings
import wx


class Canvas(wx.Window):

    def __init__(self, parent):
        super(Canvas, self).__init__(parent, -1)
        self.parent = parent
        self.isResized = True

        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_LEFT_DCLICK, self.OnDblClick)
        self.Bind(wx.EVT_LEFT_UP, self.OnClick)

    def OnDblClick(self, event):
        # test for Clipboard operation not complete, very fast repeated DblClick
        if wx.TheClipboard.Open():  # pragma: no cover
            wx.TheClipboard.SetData(wx.BitmapDataObject(self.buff))
            wx.TheClipboard.Close()

    def OnClick(self, event):
        self.parent.sensor.SetFocus()

    def OnSize(self, event):
        self.isResized = True
        event.Skip()

    def OnIdle(self, event):
        if self.isResized:
            self.InitBuffer()
            self.Refresh(False)

    def OnPaint(self, event):
        wx.BufferedPaintDC(self, self.buff)

    def InitBuffer(self):
        self.pixelX, self.pixelY = self.GetClientSize()

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.buff = wx.EmptyBitmap(self.pixelX, self.pixelY)

        dc = wx.BufferedDC(None, self.buff)
        dc.SetBackground(wx.Brush("black"))
        dc.Clear()
        self.Draw(dc)
        self.isResized = False

    def pix2mm(self, n):
        return int(self.parent.size_mm * float(n) / self.pixelX)

    def pix2x(self, n):
        return self.parent.size_x * float(n) / self.pixelX

    def x2pix(self, n):
        return int(round(float(n) * self.pixelX / self.parent.size_x))

    def pix2y(self, n):
        y = (self.parent.y_max - self.parent.y_min)
        return y * float(self.pixelY - n) / self.pixelY + self.parent.y_min

    def y2pix(self, n):
        y = (self.parent.y_max - self.parent.y_min)
        return self.pixelY - int(round(self.pixelY * (n - self.parent.y_min) / y))

    def draw_segment(self, dc, pt1, pt2, is_diskret=False):
        x1 = self.x2pix(pt1[0])
        y1 = self.y2pix(pt1[1])
        x2 = self.x2pix(pt2[0])
        y2 = self.y2pix(pt2[1])

        if is_diskret:
            dc.DrawLine(x1, y1, x2, y1)
            dc.DrawLine(x2, y1, x2, y2)
        else:
            dc.DrawLine(x1, y1, x2, y2)

    def Draw(self, dc):
        for chart in self.parent.charts:
            params = self.parent.charts[chart]
            color = params.get('color', 'white')
            thickness = params.get('thickness', 1)
            dc.SetPen(wx.Pen(color, thickness, wx.SOLID))

            line = list(enumerate(self.parent.data[chart]))
            prev_pt = line[0]
            for pt in line[1:]:
                self.draw_segment(dc, prev_pt, pt)
                prev_pt = pt

            # last segment
            self.draw_segment(dc, prev_pt, (self.pixelX, prev_pt[1]))

        for p in self.parent.projections:
            params = self.parent.projections[p]
            color = params.get('color', 'white')
            thickness = params.get('thickness', 1)
            dc.SetPen(wx.Pen(color, thickness, wx.SOLID))

            x = self.x2pix(p)
            y1 = 0
            y2 = self.pixelY

            dc.DrawLine(x, y1, x, y2)

        color = 'yellow'
        thickness = 1
        dc.SetPen(wx.Pen(color, thickness, wx.SOLID))

        if self.parent.is_sensor:

            values = map(
              lambda base, value: value - base + self.parent.y_min,
              self.parent.data[0],
              self.parent.data[self.parent.sensor.GetValue()]
            )
            min_val = values[0]
            max_val = values[0]

            for v in values:
                if v < min_val:
                    min_val = v
                if v > max_val:
                    max_val = v

            y_min = self.parent.y_min
            y_max = self.parent.y_max

            shft = int((max_val - min_val) / 20)
            if shft < 1:
                shft = 1

            self.parent.y_min = min_val - shft
            self.parent.y_max = max_val + shft

            line = list(enumerate(values))
            prev_pt = line[0]
            for pt in line[1:]:
                self.draw_segment(dc, prev_pt, pt)
                prev_pt = pt

            # last segment
            self.draw_segment(dc, prev_pt, (self.pixelX, prev_pt[1]))

            self.parent.y_min = y_min
            self.parent.y_max = y_max

            dc.SetTextForeground(color)
            dc.SetTextBackground("black")

            txt = "{}".format(max_val)
            dc.DrawText(txt, 0, 0)
            w, h = dc.GetTextExtent(txt)

            txt = "{}".format(min_val)
            dc.DrawText(txt, 0, self.pixelY - w)

            txt = "{}".format(max_val - min_val)
            dc.DrawText(txt, 0, self.pixelY / 2 - w / 2)

        if self.parent.mirror and self.parent.mirror.is_sensor:

            x = self.x2pix(self.parent.mirror.sensor.GetValue())
            y1 = 0
            y2 = self.pixelY

            dc.DrawLine(x, y1, x, y2)
