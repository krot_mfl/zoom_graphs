import os
import json
import gettext
import wx

import images
from form.main import Window
from zoom import Zoom


def app_root():
    try:
        return os.path.dirname(os.path.abspath(__file__))
    except NameError:  # pragma: no cover
        # We are the main py2exe script, not a module
        import sys
        return os.path.dirname(os.path.abspath(sys.argv[0]))


class Application(object):

    def __init__(self, lang, data_file):

        self.zoom = Zoom(json.loads(open(data_file, 'rt').read()))

        gettext.translation(
          'messages',
          os.path.join(app_root(), 'locale'),
          languages=[lang]
        ).install()

        self.app = wx.App()
        self.main_window = Window(images.gpasico.GetIcon(), _("Graphs of sensors"), self.zoom)

    def start(self):
        self.main_window.Show()
        self.app.MainLoop()


if __name__ == "__main__":  # pragma: no cover
    import sys
    if len(sys.argv) > 1:
        Application('ru', sys.argv[1]).start()
    else:
        print "Usage: zoom_graphs datafile.json"
