import wx
from control.graphs import Graphs
from config import Config


class Window(wx.Frame):

    def __init__(self, icon, title, zoom):
        super(Window, self).__init__(None, -1, title)

        self.zoom = zoom
        if icon:
            self.SetIcon(icon)

        # set 5% shifts for borders
        shft = int((self.zoom.val_max - self.zoom.val_min) / 20)
        if shft < 1:
            shft = 1

        self.vmin = self.zoom.val_min - shft
        self.vmax = self.zoom.val_max + shft

        self.statusbar = self.CreateStatusBar()
        self.sp = wx.SplitterWindow(self)

        self.hor_graphs = Graphs(
          self.sp,
          True,
          self.zoom.data,
          self.vmin, self.vmax,
          self.zoom.krot["sizeX"],
          self.zoom.selection.get("y_top", None),
          self.zoom.selection.get("calc_row", None),
          self.zoom.selection.get("y_bottom", None),
        )
        self.ver_graphs = Graphs(
          self.sp,
          False,
          self.zoom.rotated(),
          self.vmin, self.vmax,
          self.zoom.krot["sizeY"],
          self.zoom.selection.get("x_left", None),
          self.zoom.selection.get("calc_col", None),
          self.zoom.selection.get("x_right", None),
        )

        self.hor_graphs.mirror = self.ver_graphs
        self.ver_graphs.mirror = self.hor_graphs

        if self.zoom.selection:
            self.hor_graphs.set_sel_calcked(True)
            self.hor_graphs.set_sel_top(True)
            self.hor_graphs.set_sel_bottom(True)

            self.ver_graphs.set_sel_calcked(True)
            self.ver_graphs.set_sel_top(True)
            self.ver_graphs.set_sel_bottom(True)

        self.sp.SplitHorizontally(self.hor_graphs, self.ver_graphs)

        self.cfg = Config(self)
        self.cfg.restore()

        # get named colors list
        # import wx.lib.colourdb as wb
        # print wb.getColourList()
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, event):
        self.cfg.save()
        event.Skip()
