import os
import sys

if __name__ == '__main__':

    if len(sys.argv) < 2:
        raise Exception(
          "Usage: tests.py project_path [fully.qualified.names.of.the.tests] [verbose]\n"
          "Example:\n"
          "tests.py source test.my_project.test_module"
        )
    sys.path.insert(1, os.path.join(os.getcwd(), sys.argv[1]))

    import unittest
    verbose = 1
    suite = None
    loader = unittest.TestLoader()
    buf = True

    if "verbose" in sys.argv:
        verbose = 2

    if (len(sys.argv) > 2) and (sys.argv[2] not in ["verbose"]):
        suite = loader.loadTestsFromNames([sys.argv[2]])
        buf = False
    else:
        suite = loader.discover('tests')

    sys.exit(
      0 if unittest.TextTestRunner(verbosity=verbose, buffer=buf).run(suite).wasSuccessful() else 1
    )
