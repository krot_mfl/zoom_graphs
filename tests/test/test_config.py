# python tests.py ../source test.test_config
import os
from shutil import copyfile
from shutil import move

from config import Config
from . import TestCaseBase


class TestCaseConfig(TestCaseBase):

    def test_dflt(self):
        # check exist
        ini_name = os.path.join(os.path.expanduser("~"), 'zoom_graphs.ini')
        ini_file_exists = os.path.exists(ini_name)

        if ini_file_exists:
            copyfile(ini_name, ini_name + ".bkp")
            os.remove(ini_name)

        cfg = Config(self.app.main_window)
        self.assertNotEqual(cfg.ini, '')
        cfg.restore()
        self.assertNotEqual(self.app.main_window.GetSize(), (-1, -1))
        # self.assertEqual(self.app.main_window.GetPosition(), None)

        cfg.save()
        cfg = Config(self.app.main_window)
        self.assertNotEqual(cfg.ini, '')
        cfg.restore()
        self.assertNotEqual(self.app.main_window.GetSize(), (-1, -1))
        # self.assertEqual(self.app.main_window.GetPosition(), None)

        os.remove(ini_name)
        if ini_file_exists:
            move(ini_name + ".bkp", ini_name)
