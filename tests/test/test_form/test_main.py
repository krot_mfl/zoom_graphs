# python tests.py ../source test.test_form.test_main
import wx

from zoom import Zoom
from form.main import Window

from . import TestCaseForm


class TestCaseMain(TestCaseForm):

    def test_Window(self):
        self.assertTrue(self.app.main_window)
        dlt = int((self.app.zoom.val_max - self.app.zoom.val_min) / 20)
        if dlt < 1:
            dlt = 1

        vmin = "{}".format(self.app.zoom.val_min - dlt)
        vmax = "{}".format(self.app.zoom.val_max + dlt)
        diap = "{}".format(self.app.zoom.val_max - self.app.zoom.val_min + 2 * dlt)

        h = self.app.main_window.hor_graphs
        v = self.app.main_window.ver_graphs

        self.assertEqual(h.canvas_panel.top_border.GetLabel(), vmax)
        self.assertEqual(h.canvas_panel.low_border.GetLabel(), vmin)
        self.assertEqual(h.canvas_panel.diapazon.GetLabel(), diap)

        self.assertEqual(v.canvas_panel.top_border.GetLabel(), vmax)
        self.assertEqual(v.canvas_panel.low_border.GetLabel(), vmin)
        self.assertEqual(v.canvas_panel.diapazon.GetLabel(), diap)

    # https://wxpython.org/Phoenix/docs/html/wx.Event.html#wx-event
    def test_small_zoom(self):
        data = {
          "dataType": "HOLL",
          "magnetID": "EPRO700",
          "isInside": 0,
          "sizeX": 330,
          "sizeY": 716,
          "operatorID": "26A5B64D95447269BED200360DB0F0B2",
          "recordID": "C47B8A89A2509DDA279946AB264D279A",
          "recordPosition": 46290,
          "user_h0": "",
          "dataRow": [
            [100, 101, 102],
            [100, 101, 102],
            [100, 101, 102]
          ],
          "zoomDataFormat": 1
        }
        w = Window(None, "test", Zoom(data))
        self.assertEqual(w.vmin, 99)
        self.assertEqual(w.vmax, 103)

    def test_OnClose(self):
        self.assertTrue(self.app.main_window)
        self.app.main_window.OnClose(wx.CommandEvent())
