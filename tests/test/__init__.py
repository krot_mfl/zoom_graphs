import os

from app import Application
from unittest import TestCase


class TestCaseBase(TestCase):

    def setUp(self):
        TestCase.setUp(self)
        self.data_file = os.path.join('fixtures', 'default.json')
        self.app = Application('en', self.data_file)
