# python tests.py ../source test.test_zoom
import os
import json

from zoom import Zoom
from . import TestCaseBase


class TestCaseZoom(TestCaseBase):

    def test_min_max(self):
        self.assertEqual(self.app.zoom.val_min, 565)
        self.assertEqual(self.app.zoom.val_max, 743)

    def test_data(self):
        self.assertEqual(
          self.app.zoom.data[0], [
            639, 640, 641, 641, 639, 637, 635, 633, 634, 633, 632, 631, 631, 630, 630, 629, 629,
            629, 629, 630, 632, 633, 632, 631, 631, 630, 630, 630, 630, 631, 631, 628, 627, 626,
          ]
        )

    def test_rotated(self):
        self.assertEqual(self.app.zoom.rotated()[0][:5], [639, 638, 654, 651, 636])

    def test_wrong_sel_borders(self):
        data_file = os.path.join('fixtures', 'wrong_sel_borders.json')
        z = Zoom(json.loads(open(data_file, 'rt').read()))

        self.assertTrue(z.selection["x_right"] < len(z.data[0]))
        self.assertTrue(z.selection["y_bottom"] < len(z.data))
