# python tests.py ../source test.test_control.test_canvas
import os
import wx
from . import TestCaseControl
from app import Application


class TestCaseCanvas(TestCaseControl):

    def setUp(self):
        TestCaseControl.setUp(self)
        self.canvas = self.app.main_window.hor_graphs.canvas

    def test_OnPaint(self):
        event = wx.PaintEvent(wx.wxEVT_PAINT)
        event.SetEventObject(self.canvas)
        self.canvas.GetEventHandler().ProcessEvent(event)

    def test_OnIdle(self):
        event = wx.IdleEvent()
        event.SetEventObject(self.canvas)
        self.canvas.GetEventHandler().ProcessEvent(event)
        self.canvas.GetEventHandler().ProcessEvent(event)  # canvas.isResized == False

    def test_OnDblClick(self):
        event = wx.MouseEvent()
        event.SetEventObject(self.canvas)
        self.canvas.OnDblClick(event)

    def test_OnClick(self):
        event = wx.MouseEvent()
        event.SetEventObject(self.canvas)
        self.canvas.OnClick(event)

    # test draw_segment for parameter is_diskret=True
    def test_draw_segment(self):
        buff = wx.EmptyBitmap(50, 50)
        dc = wx.BufferedDC(None, buff)
        pt1 = wx.Point(10, 10)
        pt2 = wx.Point(20, 20)
        is_diskret = True

        self.canvas.draw_segment(dc, pt1, pt2, is_diskret)

    def test_Draw_if_shft_more_then_1(self):
        app = Application('en', os.path.join('fixtures', 'air_test.json'))

        wx.CallAfter(wx.Exit)
        app.start()

    def test_Draw_is_sensor_Fals(self):
        buff = wx.EmptyBitmap(50, 50)
        dc = wx.BufferedDC(None, buff)
        self.canvas = self.app.main_window.hor_graphs.canvas

        if self.canvas.parent.is_sensor:
            self.canvas.parent.is_sensor = False

        self.canvas.Draw(dc)
