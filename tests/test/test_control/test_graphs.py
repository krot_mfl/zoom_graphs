# python tests.py ../source test.test_control.test_graphs
import wx
from . import TestCaseControl


class TestCaseGraphs(TestCaseControl):

    def test_OnMotion(self):
        hor_graphs = self.app.main_window.hor_graphs
        ver_graphs = self.app.main_window.ver_graphs
        event = wx.MouseEvent(wx.wxEVT_MOTION)

        event.SetEventObject(ver_graphs.canvas)
        event.SetPosition((10, 15))
        ver_graphs.canvas.GetEventHandler().ProcessEvent(event)

        self.assertTrue(len(ver_graphs.pos_value.GetLabel()) > 0)
        self.assertTrue(len(ver_graphs.val_value.GetLabel()) > 0)

        event.SetEventObject(hor_graphs.canvas)
        event.SetPosition((20, 25))
        hor_graphs.canvas.GetEventHandler().ProcessEvent(event)

        self.assertTrue(len(hor_graphs.pos_value.GetLabel()) > 0)
        self.assertTrue(len(hor_graphs.val_value.GetLabel()) > 0)

    def test_add_chart(self):
        hor = self.app.main_window.hor_graphs
        hor.del_chart(0)
        hor.del_projection(0)

        self.assertNotEqual(hor.charts, {})
        self.assertNotEqual(hor.projections, {})

        hor.del_chart(0)
        hor.del_projection(0)

        self.assertNotEqual(hor.charts, {})
        self.assertNotEqual(hor.projections, {})

        hor.add_chart(0)
        hor.add_projection(0)

        self.assertEqual(hor.charts[0], {})
        self.assertEqual(hor.projections[0], {})

        hor.del_chart(0)
        hor.del_projection(0)

        self.assertNotIn(0, hor.charts)
        self.assertNotIn(0, hor.projections)

        with self.assertRaisesRegexp(Exception, 'Wrong add_chart row_index:'):
            hor.add_chart(-1)

        with self.assertRaisesRegexp(Exception, 'Wrong add_projection col_index:'):
            hor.add_projection(-1)

        hor.add_chart(0, params={'color': 'red'})
        self.assertIn(0, hor.charts)

        hor.add_projection(0, params={'color': 'red'})
        self.assertIn(0, hor.projections)

        hor.add_chart(0)
        self.assertTrue(hor.charts[0])

        hor.add_projection(0)
        self.assertTrue(hor.projections[0])

    def test_pix2x(self):
        hor = self.app.main_window.hor_graphs

        x100 = hor.canvas.pix2x(100)
        x10 = hor.canvas.pix2x(10)

        self.assertEqual(hor.canvas.x2pix(x100), 100)
        self.assertEqual(hor.canvas.x2pix(x10), 10)

        x100 = hor.canvas.pix2y(100)
        x10 = hor.canvas.pix2y(10)

        self.assertEqual(hor.canvas.y2pix(x100), 100)
        self.assertEqual(hor.canvas.y2pix(x10), 10)

    def test_OnYellow(self):
        self.app.main_window.hor_graphs.OnYellow(wx.CommandEvent())

    def test_OnBorderTop(self):
        self.app.main_window.hor_graphs.OnBorderTop(wx.CommandEvent())

    def test_OnBorderBottom(self):
        self.app.main_window.hor_graphs.OnBorderBottom(wx.CommandEvent())

    def test_OnSelectionCalck(self):
        self.app.main_window.hor_graphs.OnSelectionCalck(wx.CommandEvent())

    def test_OnSelectionTop(self):
        self.app.main_window.hor_graphs.OnSelectionTop(wx.CommandEvent())

    def test_OnSelectionBottom(self):
        self.app.main_window.hor_graphs.OnSelectionBottom(wx.CommandEvent())

    def test_OnSensor(self):
        self.app.main_window.hor_graphs.OnSensor(wx.CommandEvent())

    def test_OnSettings(self):
        self.app.main_window.hor_graphs.show_settings.SetValue(False)
        self.app.main_window.hor_graphs.OnSettings(wx.CommandEvent())

        self.app.main_window.hor_graphs.show_settings.SetValue(True)
        self.app.main_window.hor_graphs.OnSettings(wx.CommandEvent())

    # test set_selection for parameter is_graph_draw=True then is_on = False
    def test_set_selection(self):
        self.show_Test = wx.CheckBox()

        self.app.main_window.hor_graphs.set_selection(
          False, {'color': 'green'}, 1, self.show_Test, is_graph_draw=True
        )
