# python tests.py ../source test.test_app
# https://wiki.wxpython.org/Unit%20Testing%20with%20wxPython
import os
import wx
from app import Application

from . import TestCaseBase


class TestCaseApp(TestCaseBase):

    def test_Application(self):
        title = 'Graphs of sensors'
        self.assertEqual(Application('en', self.data_file).main_window.GetTitle(), title)
        self.assertNotEqual(Application('ru', self.data_file).main_window.GetTitle(), title)

        with self.assertRaisesRegexp(IOError, 'No such file or directory'):
            Application('en', 'not_exist')

    def test_start(self):
        wx.CallAfter(wx.Exit)
        self.app.start()

    def test_wrong_sel_borders(self):
        Application('en', os.path.join('fixtures', 'wrong_sel_borders.json'))
