.PHONY: all clean messages flake8 tests media release setup run lint

ifeq ($(OS),Windows_NT)
include windows.mk
else
include linux.mk
endif

MSVCDLL = MSVCP90.dll
CLEAR_PYC = $(PYTHON) clear_pyc.py
S = source
T = tests
LOCALEDIR = $(S)/locale
ICO = $(S)/media/gpas.ico
IMAGES = $(ICO)
IMAGEPY = $(S)/images.py
PROJECT = source
COVERAGE = python -m coverage
TESTS = $(T)/tests.py $(PROJECT)

all: run

run: flake8 media $(IMAGEPY)
	$(PYTHON) $(S)/app.py $(T)/fixtures/default.json

messages:
	$(PYBABEL) extract -F babel.cfg -o $(LOCALEDIR)/messages.pot .
	make -C $(LOCALEDIR) messages

media:
	make -C $(LOCALEDIR)

flake8:
	$(PYTHON) -m flake8 --max-line-length=110 --exclude=images.py --builtins="_" $(S)
	$(PYTHON) -m flake8 --max-line-length=110 $(T)

lint:
	$(PYTHON) -m pylint $(S) $(T)/test

coverage:
	$(COVERAGE) run $(TESTS)

html:
	$(COVERAGE) html --skip-covered

# sudo find / | grep google-cloud-sdk
# make tests >log_file 2>&1
tests: clean flake8 lint media $(IMAGEPY) coverage html
	$(COVERAGE) report --skip-covered

release: clean flake8 media $(MSVCDLL) gpas.ico $(IMAGEPY)
	$(PYTHON) setup.py py2exe

$(MSVCDLL):
	$(CP) $(GNU)\$(MSVCDLL) .

gpas.ico:
	$(CP) $(ICO) .

$(IMAGEPY): $(IMAGES)
	$(IMG2PY) -n gpasico $(ICO) $(IMAGEPY)

clean:
	@-$(DEL) $(LOCALEDIR)/messages.pot >nul
	@make -C $(LOCALEDIR) clean
	@$(CLEAR_PYC) $(T)/test
	@$(CLEAR_PYC) $(S)
	@-$(DEL) log_file $(T)/.coverage $(T)/log_file >nul
	@-$(DEL) -R -f htmlcov
	@-$(DEL) -R -f build
	@-$(DEL) -R -f dist
	@-$(DEL) $(MSVCDLL) gpas.ico $(IMAGEPY)

setup:
	@$(PYTHON) -m pip install -r requirements.txt
